/**
 * @fileOverview Расширение класса [jsonrpc.Peer]{@link external:jsonrpc.Peer}, поддерживающий соединение с удалённым
 * сервером с помощью [SockJS]{@link external:SockJS} соединенеия. Благодаря тому, что соединение двунаправленное,
 * то на клиенте возможно выполнение процедур сызванных сервером, в связи с чем на клиенте возможна реализация
 * контроллера. Данный класс не включает в себя контроллер, но как и в родительской библиотеке имеет интерфейс для
 * подключения контроллера через реализацию метода
 * [jsonrpc._getRequestedProcedure]{@link external:jsonrpc.Peer#_getRequestedProcedure}
 * @ignore
 */

/**
 * Событие говорит о том, что клиент установил соединение с сервисом, это может произойти в результате вызова метода
 * [Peer#connect]{@link module:main.Peer#connect}
 *
 * @event module:main.Peer#connect
 * @type {Undefined}
 */

/**
 * Событие говорит о том, что клиент утратил соединение с сервисом, это может произойти в результате вызова метода
 * [Peer#disconnect]{@link module:main.Peer#disconnect}, либо в результате обрыва соединения по
 * любой другой причине. Если соединение было разорвано, и клиент находится в режиме восстановления соединения, -
 * параметр [options.reconnect_timeout]{@link module:main.Peer~Options} имеет
 * положительное значение и метод [Peer#disconnect]{@link module:main.Peer#disconnect} ещё не был
 * вызван, то будет предпринятат попытка установить новое соединение. После того, как соединение будет восстановлено
 * будет сгенерировано событие [connect]{@link module:main.Peer#event:connect}
 *
 * @event module:main.Peer#disconnect
 * @type {Undefined}
 */

/**
 * @ignore
 */
var inherits = require('util').inherits;

/**
 * @ignore
 */
var Peer = require('node-jsonrpc/lib/peer');

/**
 * @ignore
 */
var main = require('../');

/**
 * @ignore
 */
var SockJS = require('./sockjs');

/**
 * Список параметров клиента по умолчанию
 *
 * @alias module:main.Peer~Options
 * @typedef
 * @type {Object}
 * @property {String} url URL для подключения к SockJS серверу
 * @property {?Number} [reconnect_timeout=-1] Таймаут автоматического восстановления соединения (см.
 * [Peer#_reconnect]{@link module:main.Peer#_reconnect}). При потере соединения попытка
 * восстановить его будет предпринята по прошествии данного времени. Для того, чтобы отключить автоматическое
 * восстановление соединения укажите значения таймаута равное <code>-1</code>.
 * @property {?Number} [request_timeout=60] Таймаут выполнения запроса (сек.), если после отправки запроса на
 * выполнение процедуры, от удалённой стороны не поступило ответа в течение этого времени, то мы считаем,
 * что запрос провалился с ошибкой <code>"Request timeout"</code> (код <code>-32001</code>).
 * @property {?Object} [sockjs={devel: false, debug: false, protocols_whitelist: null, rtt: 500}] Параметры SockJS
 * соединения
 * @property {Boolean} [sockjs.devel=false] Режим разработчика. Влючение данного параметра способствует отключению
 * кэша HTTP запросов, производимых библиотекой SockJS. Данный параметр запроещено использовать в продакш окркжении.
 * @property {Boolean} [sockjs.debug=false] Включение лога библиотеки SockJS, по умолчанию используется консоль
 * браузера. Данный параметр запроещено использовать в продакш окркжении.
 * @property {?String[]} [sockjs.protocols_whitelist=null] Список протоколов, доступных для установления SockJS
 * подключения. Доступны протоколы:<ol>
 *     <li><code>"websocket"</code></li>
 *     <li><code>"xdr-streaming"</code></li>
 *     <li><code>"xhr-streaming"</code></li>
 *     <li><code>"iframe-eventsource"</code></li>
 *     <li><code>"iframe-htmlfile"</code></li>
 *     <li><code>"xdr-polling"</code></li>
 *     <li><code>"xhr-polling"</code></li>
 *     <li><code>"iframe-xhr-polling"</code></li>
 *     <li><code>"jsonp-polling"</code></li>
 * </ol>
 * @property {Number} [sockjs.rtt=500] Round trip time. Допустимное время для обмена данными между сервером и клиентом
 * по пути клиент-сервер-клиент. Используется для задания таймаута установления соединения библиотекой SockJS.
 */
var Options = {
    url: null,
    reconnect_timeout: -1,
    request_timeout: 60,
    sockjs: {
        devel: false,
        debug: false,
        protocols_whitelist: null,
        rtt: 500
    }
};

/**
 * Расширение класса [jsonrpc.Peer]{@link external:jsonrpc.Peer}, поддерживающий соединение с удалённым сервером с
 * помощью [SockJS]{@link external:SockJS} соединенеия. Благодаря тому, что соединение двунаправленное, то на клиенте
 * возможно выполнение процедур сызванных сервером, в связи с чем на клиенте возможна реализация контроллера. Данный
 * класс не включает в себя контроллер, но как и в родительской библиотеке имеет интерфейс для подключения контроллера
 * через реализацию метода [jsonrpc._getRequestedProcedure]{@link external:jsonrpc.Peer#_getRequestedProcedure}
 *
 * @alias module:main.Peer
 * @abstract
 * @constructor
 * @augments external:jsonrpc.Peer
 * @param {module:main.Peer~Options} options
 */
module.exports = exports = function (options) {
    if (!(this instanceof exports)) {
        return new exports(options);
    }
    if (typeof options !== 'object' || options === null) {
        throw new TypeError('options must be a non-null object');
    }
    if (typeof options.url !== 'string') {
        throw new TypeError('options.url must be a valid URL address');
    }
    if (typeof options.reconnect_timeout === 'undefined') {
        options.reconnect_timeout = Options.reconnect_timeout;
    }
    if (typeof options.request_timeout === 'undefined') {
        options.request_timeout = Options.request_timeout;
    }
    if (typeof options.sockjs === 'undefined') {
        options.sockjs = {};
        options.sockjs.devel = Options.sockjs.devel;
        options.sockjs.debug = Options.sockjs.debug;
        options.sockjs.protocols_whitelist = Options.sockjs.protocols_whitelist;
        options.sockjs.rtt = Options.sockjs.rtt;
    } else {
        if (typeof options.sockjs.devel === 'undefined') {
            options.sockjs.devel = Options.sockjs.devel;
        }
        if (typeof options.sockjs.debug === 'undefined') {
            options.sockjs.debug = Options.sockjs.debug;
        }
        if (typeof options.sockjs.protocols_whitelist === 'undefined') {
            options.sockjs.protocols_whitelist = Options.sockjs.protocols_whitelist;
        }
        if (typeof options.sockjs.rtt === 'undefined') {
            options.sockjs.rtt = Options.sockjs.rtt;
        }
    }
    Peer.call(this, options);
    for (var i in options) {
        this._options[i] = options[i];
    }
};

inherits(exports, Peer);

/**
 * Метод производит отправку JSON-RPC сообщений из буффера. Если сообщение - это запрос типа
 * [уведомление]{@link http://www.jsonrpc.org/specification#notification}, то после того, как сообщение отправлено
 * для запроса будет зафиксирован положительный результат выполнения (см.
 * [jsonrpc.Request#finish]{@link external:jsonrpc.Request#finish}).
 *
 * @alias module:main.Peer#send
 * @override external:jsonrpc.Peer#send
 * @returns {Undefined}
 */
exports.prototype.send = function () {
    if (this.isConnected()) {
        while(this._sendBuffer.length > 0) {
            var message = this._sendBuffer.shift();
            this._socket.send(message.toString());
            if (message instanceof main.Request && message.id === null) {
                message.finish();
            }
        }
    }
};

/**
 * Проверяет статус соединения, если соединение установлено, то возвращает <code>true</code>, <code>false</code>
 * в противном случае
 *
 * @alias module:main.Peer#isConnected
 * @returns {Boolean}
 */
exports.prototype.isConnected = function () {
    return this._socket !== null && this._socket.readyState === SockJS.OPEN;
};

/**
 * Осуществляет подключение к сервису по указанному в настройках адресу
 * (см. [Peer~Options.url]{@link module:main.Peer~Options.url}). После вызова метода при неудачной
 * попытке соединения (или разрыве соединения) клиент будет осуществлять попытку повторного соединения. Повторное
 * соединение осуществляется с интервалом заданным параметром
 * [Peer~Options.reconnect_timeout]{@link module:main.Peer~Options.reconnect_timeout}. Если для
 * данного параметра задано отрицательное значение, то повторные попытки установления соединения не будут осуществлены.
 *
 * @alias module:main.Peer#connect
 * @returns {Undefined}
 * @fires module:main.Peer#connect
 */
exports.prototype.connect = function () {
    this._reconnect = true;
    this._connect();
};

/**
 * Осуществляет отключение от сервиса, или прекращение пыпыток повторного соединения (в случае, если соединение не
 * было установлено).
 *
 * @alias module:main.Peer#disconnect
 * @returns {Undefined}
 * @fires module:main.Peer#disconnect
 */
exports.prototype.disconnect = function () {
    this._reconnect = false;
    if (this._connectTimer !== null) {
        clearTimeout(this._connectTimer);
        this._connectTimer = null;
    }
    if (this._socket !== null) {
        this._socket.close();
    }
};

/**
 * [SockJS]{@link external:SockJS} соединение
 *
 * @alias module:main.Peer#_socket
 * @private
 * @type {external:SockJS}
 */
exports.prototype._socket = null;

/**
 * Флаг, показывающий что делать при разрыве соединения или ошибке утсановления соединения. Изменяется при вызове
 * методов [Peer#connect]{@link module:main.Peer#connect} и
 * [Peer#disconnect]{@link module:main.Peer#disconnect}
 *
 * @alias module:main.Peer#_reconnect
 * @private
 * @type {Boolean}
 */
exports.prototype._reconnect = false;

/**
 * Таймер для осуществления переподключения (восстановления соединения)
 *
 * @alias module:main.Peer#_connectTimer
 * @private
 * @type {Number}
 */
exports.prototype._connectTimer = null;

/**
 * Создание подключения, именно этот метод осуществляет установление соединения. Публичный метод
 * [Peer#connect]{@link module:main.Peer#connect} лишь активирует механизм переподключения (см.
 * [Peer#_reconnect]{@link module:main.Peer#_reconnect}) и вызывает данный метод.
 *
 * @alias module:main.Peer#_connect
 * @private
 * @returns {Undefined}
 */
exports.prototype._connect = function () {
    var self = this;
    if (self._connectTimer !== null) {
        clearTimeout(self._connectTimer);
        self._connectTimer = null;
    }
    self._socket = new SockJS(self._options.url, undefined, self._options.sockjs);
    self._socket.addEventListener('open', function () {
        if (self._socket === this) {
            self.emit('connect');
            self.send();
        }
    });
    self._socket.addEventListener('message', function (message) {
        if (self._socket === this) {
            self._acceptMessage(message.data);
        } else {
            try {
                var jsonrpc_message = main.parseMessage(message.data);
                if (jsonrpc_message instanceof main.Request) {
                    this.send((new main.Response(null, new main.Error(-32002, "Connection closed"))).toString());
                } else {
                    self._acceptResponse(jsonrpc_message);
                }
            } catch (error) {
                this.send((new main.Response(null, error)).toString());
            }
        }
    });
    self._socket.addEventListener('close', function () {
        var id, index;

        // void all incoming messages which has been accepted by closed SockJS connection
        for (id in self._incomingRequests) {
            self._incomingRequests[id].finish(new main.Error(-32002, "Connection closed"));
        }
        // void all outgoing messages which has been sent by closed SockJS connection
        for (id in self._outgoingRequests) {
            index = self._sendBuffer.indexOf(self._outgoingRequests[id]);
            if (index < 0) {
                self._outgoingRequests[id].finish(new main.Error(-32002, "Connection closed"));
            }
        }
        // reconnect
        if (self._reconnect && self._options.reconnect_timeout > -1) {
            if (self._connectTimer !== null) {
                clearTimeout(self._connectTimer);
                self._connectTimer = null;
            }
            self._connectTimer = setTimeout(function () {
                self._connect();
            }, self._options.reconnect_timeout);
        }

        if (self._socket === this) {
            self._socket = null;
            self.emit('disconnect');
        }
    });
};