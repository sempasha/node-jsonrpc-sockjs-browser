SockJS JSON-RPC клиент для браузера
---

# О проекте

Абстрактный браузерный клиент [JSON-RPC](http://www.jsonrpc.org/specification) сервиса, работающий с сервисом по
[SockJS](https://github.com/sockjs/sockjs-client) соединению  (по возможности использует
[websocket](https://developer.mozilla.org/ru/docs/WebSockets) соединения, или comet соединения, в случае недоступности
websocket), основан на библиотеке [node-jsonrpc](https://bitbucket.org/sempasha/node-jsonrpc).
Так как SockJS соединение позволяет клиенту отвечать на запросы сервиса, то в клиенте предполагается наличие
контроллера. В данном клиенте нет реализации контроллера, но основной класс `main.Peer` (унаследован от `jsonrpc.Peer`)
модуля позволяет подключать контроллер через реализацию метода `jsonrpc.Peer#_getRequestedProcedure`

# Установка

Библиотека доступна на сервисе [Bitbucket](https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser). Есть 3 варианта
её загрузки/установки:

 1. Через менеджер пакетов [NPM](#NPM);
 1. Через [Git](#Git);
 1. Загрузка [Zip архива](Zip).

В репозитории для каждой минорной версии библиотеки есть ветка с названием соответствующим этой версии. Название ветки
задаётся в формате `v{major}.{minor}`, где `{major}` - соответствует номеру мажорной версии библиотеки, а `{minor}` -
минорной. (библиотека поддерживает [симантичесую систему определения версии](http://semver.org/)). Так же в главной
ветке `master` репозитория всегда находится самая свежая версия библиотеки. Примеры:

 1. Ветка `v0.1` - содержит библиотеку версии `0.1.x`, где `x` - макисальная версия патча среди пакета версий 0.1.*;
 1. Ветка `v0.5` - содержит библиотеку версии `0.5.x`, `x` - аналогично пункту 1;
 1. `master` - содержит библиотеку самой последней версии.

## NPM

Установка [NPM](https://www.npmjs.org/) пакета последней версии с
[Bitbucket](https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser):

```
$ npm install git+https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser.git --save
```

В этом случае произойдёт установка последней актуальной версии пакета. Для того, чтобы установить пакет определённой
версии - укажите соответствую ветку в адресе:

```
$ npm install git+https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser.git#v{major}.{minor} --save
```

В качестве альтернативы можно использовать установку из [tarball](http://en.wikipedia.org/wiki/Tarball):

```
$ npm install https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser/get/master.tar.gz --save
```

или

```
$ npm install https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser/get/v{minor}.{major}.tar.gz --save
```

## Git

```
$ git clone git@bitbucket.org:sempasha/node-jsonrpc-sockjs-browser.git
```

## Zip

Для загрузки последней версии загрузить
[zip мастер ветки](https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser/get/master.zip):

```
$ wget https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser/get/master.zip
```

Для загрузки специфической версии:

```
$ wget https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser/get/v{minor}.{major}.zip
```

# Тесты

Все компоненты библиотеки покрыты тестами. Запуск тестов:

```
$ npm run test
```

## Тесты в браузере

По умолчанию для выполнения тестов используется [phantomjs](http://phantomjs.org/), если же есть необходимость
 прогнать тесты вручном режиме (в любом браузере), то сначала необходимо запустить тестовый сервер:

```
$ npm run pretest
```

В этом случае сервер будет запущен на порту `9999` и будет ожидать соединения на всех доступных интерфейсах. Для того,
чтобы задать порт (например `8800`), необходимо выполнить запуск с помощью команды

```
$ node ./test/server.js --port 8800 start
```

После запуска сервера тестовая страница доступна по адресу `http://host:port`, где `host` - это имя хоста, а `port` -
это соответственно номер порта. Так например по умолчанию для доступа к тестовой странице с localhost надо использовать
URL http://localhost:9999.

После того, как тесты будут выполнены надо завершить работу тестовго сервера:

```
$ npm run posttest
```

# API

Документация по API собирается с помощью [JSDoc](http://usejsdoc.org/), для сборки можно использовать NPM run-script

```
$ npm run docs
```

По умолчанию после сборки документация доступна в папке проекта - [docs](docs/index.html).