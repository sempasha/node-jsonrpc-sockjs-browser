/* jshint ignore:start */
/**
 * @external jsonrpc
 * @desc Реализация клиент-серверного взаимодействия по протоколу
 * [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification} для JavaScript.
 * Не имеет реализации транспорта сообщений, для реализации отправки/получения JSON-RPC сообщений заложен интерфейс и
 * асбтрактные методы.
 * @see [node-jsonrpc]{@link https://bitbucket.org/sempasha/node-jsonrpc}
 */

/**
 * @class external:jsonrpc.Request
 * @desc [JSON-RPC 2.0 Запрос]{@link http://www.jsonrpc.org/specification#request_object}
 * @see [jsonrpc.Request]{@link https://bitbucket.org/sempasha/node-jsonrpc#main.Request}
 */

/**
 * @method external:jsonrpc.Request#finish
 * @desc Задаёт результат выполнения запроса. В случае возникновения ошибки пр выполнении запроса необходимо передать
 * её в параметре <code>error</code>, при этом параметр <code>result</code> должен быть пропущен (либо для него задано
 * значение <code>NULL</code>). В случае успешного выполнения запроса, в параметре <code>result</code> должен
 * содержаться результат выполнения процедуры, а параметр <code>error</code> должен быть <code>NULL</code>.
 * Для запросов типа уведомление (запросы с пустым <code>id</code>) результат выполнения всегда отсутствует.
 * После того, как будет задан результат выполнения/отправки запроса, будет произведён вызов обработчиков результата,
 * заданных с помощью метода [jsonrpc.Request#onFinish]{@link external:jsonrpc.Request#onFinish}
 * @see [jsonrpc.Request#finish]{@link https://bitbucket.org/sempasha/node-jsonrpc#main.Request%23finish}
 */

/**
 * @method external:jsonrpc.Request#isFinished
 * @desc Определяет состояние запроса - завершён/незавершён (см.
 * [jsonrpc.Request#finish]{@link external:jsonrpc.Request#finish})
 * @see [jsonrpc.Request#isFinished]{@link https://bitbucket.org/sempasha/node-jsonrpc#main.Request%23isFinished}
 */

/**
 * @class external:jsonrpc.Peer
 * @desc Абстрактный класс описывающий точку являющуюся субъектом/объектом JSON-RPC сообщения,
 * т.е. выполняющую роль либо клиента, вызывающего удалённые процедуры на сервере, либо самого сервера, на котором
 * происходит выполнение вызванных удалённых процедур. В данном классе отсутствует реализация транспорта сообщений.
 * @see [jsonrpc.Peer]{@link https://bitbucket.org/sempasha/node-jsonrpc#main.Peer}
 */

/**
 * @method external:jsonrpc.Peer#_getRequestedProcedure
 * @desc Возвращает функцию соответствующую запрошенной процедуре. Для того, чтобы задать контекст выполнения процедуры
 * в ответ необходимо вернуть объект с двум полями <code>{function: Function, context: Object}</code>
 * @see [jsonrpc.Peer#_getRequestedProcedure]{@link https://bitbucket.org/sempasha/node-jsonrpc#main.Peer%23_getRequestedProcedure}
 */

/**
 * @typedef external:jsonrpc.Peer~Options
 * @desc Список параметров для конструктора класса jsonrpc.Peer
 * @see [jsonrpc.Peer~Options]{@link https://bitbucket.org/sempasha/node-jsonrpc#main.Peer~Options}
 */

/**
 * @method external:jsonrpc.Peer#send
 * @desc Метод производит отправку JSON-RPC сообщений из буффера. Перед отправкой запроса необходимо удостовериться, что
 * запрос не был отменён (см. [jsonrpc.Request#isFinished]{@link external:jsonrpc.Request#isFinished}). Если сообщение -
 * это запрос типа [уведомление]{@link http://www.jsonrpc.org/specification#notification}, то после того,
 * как сообщение отправлено для запроса будет зафиксирован положительный результат выполнения
 * (см. [jsonrpc.Request#finish]{@link external:jsonrpc.Request#finish})
 * @see [jsonrpc.Peer#send]{@link https://bitbucket.org/sempasha/node-jsonrpc#main.Peer%23send}
 */

/**
 * @external SockJS
 * @desc Клиент, позволяющий устанавливать двунаправленное соединение между браузером и сервером (предположительно
 * [SockJS сервер для NodeJS]{@link https://github.com/sockjs/sockjs-node}), реализующий протокол
 * [SockJS]{@link https://github.com/sockjs/sockjs-protocol}. Поддерживает транспорт данных по
 * [websocket]{@link http://tools.ietf.org/html/rfc6455} или в случае недоступности websocket по XHR(XDR) Streaming,
 * XHR(XDR) Polling, JSONP Polling и т.д.
 * @see [sockjs-client]{@link https://github.com/sockjs/sockjs-client}
 */
/* jshint ignore:end */

/**
 * @fileOverview Модуль JSON-RPC является абстракцией для реализации диалога связанных неким каналом связи точек
 * в стиле удалённого вызова процедур, в качестве протокола сериализации используется
 * [JSON-RPC 2.0]{@link http://www.jsonrpc.org/specification}. При этом модуль спроектирован таким образом,
 * что не имеет реализации протокола передачи данных, таким образом для его реального применения необходимо разработать
 * его расишрение под конкретный случай передачи данных.
 * @module main
 * @augments external:jsonrpc
 */

/**
 * Расширяем модуль [node-jsonrpc]{@link external:jsonrpc}
 *
 * @type {exports}
 * @ignore
 */
module.exports = exports = require('node-jsonrpc');

/**
 * JSON парсер/сериализатор для браузеров не имеющих глобального JSON объекта
 *
 * @alias module:main.JSON
 * @type {{stringify: Function, parse: Function}}
 */
exports.JSON = typeof JSON !== 'undefined' ? JSON : require('json3');

/**
 * Расширение класса [jsonrpc.Peer]{@link external:jsonrpc.Peer}, поддерживающий соединение с удалённым сервером с
 * помощью [SockJS]{@link external:SockJS} соединенеия. Благодаря тому, что соединение двунаправленное, то на клиенте
 * возможно выполнение процедур сызванных сервером, в связи с чем на клиенте возможна реализация контроллера. Данный
 * класс не включает в себя контроллер, но как и в родительской библиотеке имеет интерфейс для подключения контроллера
 * через реализацию метода [jsonrpc._getRequestedProcedure]{@link external:jsonrpc.Peer#_getRequestedProcedure}
 *
 * @ignore
 * @alias module:main.Peer
 * @class
 */
exports.Peer = require('./lib/peer');