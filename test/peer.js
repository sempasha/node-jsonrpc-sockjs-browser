/**
 * @fileOverview Тесты класса [main.Peer]{@link module:main.Peer}
 * @ignore
 */

/* global describe, it, xit */
var assert = require('assert'),
    main = require('../'),
    url = window.location.protocol + '//' + window.location.host + '/service';

describe('main.Peer', function() {

    describe('#constructor', function () {
        it('Принимает список опций, обязательный параметр, options.url - обязательный параметр',
            function () {
                var create = function (options) {
                    return function () {
                        return new main.Peer(options);
                    };
                };
                assert.throws(create(null), TypeError);
                assert.throws(create(''), TypeError);
                assert.throws(create({}), TypeError);
                assert.throws(create({}), TypeError);
                assert.throws(create({test: url}), TypeError);
                assert.doesNotThrow(create({url: url}));
            });

        it('Если в options не переданы какие-либо параметры, их значение будет взято из списка опций по умолчанию ' +
            '{reconnect_timeout: -1, sockjs: {devel: false, debug: false, protocols_whitelist: null, rtt: 500}}',
            function () {
                var options_default = {
                        url: url,
                        reconnect_timeout: -1,
                        sockjs: {
                            devel: false,
                            debug: false,
                            protocols_whitelist: null,
                            rtt: 500
                        }
                    },
                    options_variants = [
                        {url: url},
                        {url: url, reconnect_timeout: 5000},
                        {url: url, sockjs: {devel: true}},
                        {url: url, sockjs: {debug: true}},
                        {url: url, sockjs: {protocols_whitelist: ['websocket']}},
                        {url: url, sockjs: {rtt: 1500}}
                    ],
                    test_options = function (options) {
                        var expected = {
                                url: options_default.url,
                                reconnect_timeout: options_default.reconnect_timeout,
                                sockjs: {
                                    devel: options_default.sockjs.devel,
                                    debug: options_default.sockjs.debug,
                                    protocols_whitelist: options_default.sockjs.protocols_whitelist,
                                    rtt: options_default.sockjs.rtt
                                }
                            },
                            actual = (new main.Peer(options))._options;

                        if ('url' in options) {
                            expected.url = options.url;
                        }
                        if ('reconnect_timeout' in options) {
                            expected.reconnect_timeout = options.reconnect_timeout;
                        }
                        if ('sockjs' in options) {
                            if ('devel' in options.sockjs) {
                                expected.sockjs.devel = options.sockjs.devel;
                            }
                            if ('debug' in options.sockjs) {
                                expected.sockjs.debug = options.sockjs.debug;
                            }
                            if ('protocols_whitelist' in options.sockjs) {
                                expected.sockjs.protocols_whitelist = options.sockjs.protocols_whitelist;
                            }
                            if ('rtt' in options.sockjs) {
                                expected.sockjs.rtt = options.sockjs.rtt;
                            }
                        }
                        assert.equal(actual.url, expected.url, 'options.url не совпадает с ожидаемым значением');
                        assert.equal(actual.reconnect_timeout, expected.reconnect_timeout,
                            'options.reconnect_timeout не совпадает с ожидаемым значением');
                        assert.ok(actual.sockjs instanceof Object, 'options.sockjs не является объектом');
                        assert.equal(actual.sockjs.devel, expected.sockjs.devel,
                            'options.sockjs.devel не совпадает с ожидаемым значением');
                        assert.equal(actual.sockjs.debug, expected.sockjs.debug,
                            'options.sockjs.debug не совпадает с ожидаемым значением');
                        assert.equal(actual.sockjs.protocols_whitelist, expected.sockjs.protocols_whitelist,
                            'options.sockjs.protocols_whitelist не совпадает с ожидаемым значением');
                        assert.equal(actual.sockjs.rtt, expected.sockjs.rtt,
                            'options.sockjs.rtt не совпадает с ожидаемым значением');
                    },
                    i = 0;

                for (; i < options_variants.length; i++) {
                    test_options(options_variants[i]);
                }
            });
    });

    describe('#_getRequestedProcedure', function () {
        it('Метод абстрактный и не имеет реализации',
            function () {
                assert.throws(function () {
                    (new main.Peer())._getRequestedProcedure();
                });
            });

        xit('Возвращает функцию, соответсвующую запрошенной процедуре');

        xit('Функция может быть представлена объектом вида {function: Function, context: Object}, где ' +
            'поле function сожержит функцию процедуры, а context - контекст вызова функции');
    });

    describe('#connect', function () {
        it('Осуществляет подключение к серверу, после подключения создает событие connect',
            function (done) {
                var peer = new main.Peer({url: url});

                peer.connect();
                peer.on('connect', function () {
                    peer.disconnect();
                    peer.once('disconnect', function () {
                        done();
                    });
                });
                assert.ok(peer._socket, 'SockJS подключение не создано');
            });

        it('Активирует механизм переподключения, при условии, что параметр ' +
            'options.reconnect_timeout конструктора положительный',
            function (done) {
                this.timeout(5000);

                var peer = new main.Peer({url: url, reconnect_timeout: 1000}),
                    reconnection = false;

                peer.connect();
                assert.ok(peer._reconnect, 'Механизм переподключения не активирован');
                peer.on('connect', function () {
                    if (!reconnection) {
                        reconnection = true;
                        setTimeout(function () {
                            peer._socket.close();
                        }, 0);
                    } else {
                        peer.disconnect();
                        peer.once('disconnect', function () {
                            done();
                        });
                    }
                });
            });
    });

    describe('#disconnect', function () {
        it('Осуществляет отключение от сервера',
            function (done) {
                var peer = new main.Peer({url: url});

                peer.connect();
                peer.on('connect', function () {
                    peer.disconnect();
                    assert.ok(!peer.isConnected(), 'Соединение должно быть разорвано');
                    peer.once('disconnect', function () {
                        done();
                    });
                });
            });

        it('После отключения генерируется событие disconnect',
            function (done) {
                var peer = new main.Peer({url: url});

                peer.connect();
                peer.on('connect', function () {
                    peer.disconnect();
                    peer.once('disconnect', function () {
                        done();
                    });
                });
            });

        it('Деактивирует механизм восстановления соединения',
            function (done) {
                var peer = new main.Peer({url: url});

                peer.connect();
                peer.on('connect', function () {
                    peer.disconnect();
                    assert.ok(!peer._reconnect, 'Механизм переподключения всё ещё активирован');
                    peer.once('disconnect', function () {
                        done();
                    });
                });
            });
    });

    describe('#isConnected', function () {
        it('Проверяет статус подключения к сервису, если подключение установлено, возвращает true, ' +
            'в противном случае false',
            function (done) {
                var peer = new main.Peer({url: url});

                assert.ok(!peer.isConnected(), 'SockJS соединение не создано, а метод вернул true');
                peer.connect();
                assert.ok(!peer.isConnected(), 'SockJS соединение не создано, но не подключено а метод вернул true');
                peer.on('connect', function () {
                    assert.ok(peer.isConnected(), 'SockJS соединение не создано и подключено а метод вернул false');
                    peer.disconnect();
                    assert.ok(!peer.isConnected(), 'SockJS соединение в состоянии завершения, а метода вернул true');
                    peer.once('disconnect', function () {
                        assert.ok(!peer.isConnected(), 'SockJS соединение закрыто, а метода вернул true');
                        done();
                    });
                });
            });
    });

    describe('#send', function () {
        it('Производит отправку сообщений из буфера Peer#_sendBuffer в порядке очереди',
            function (done) {
                var peer = new main.Peer({url: url}),
                    queue = [],
                    sent = [],
                    length = 10,
                    toString = function () {
                        sent.push(this);
                        delete this.toString;
                        return this.toString();
                    };

                peer.connect();
                peer.on('connect', function () {
                    var request, i, test = function (request, index) {
                        assert.strictEqual(queue[index], sent[index], 'Неверный порядок отправки');
                    };
                    for (i = 0; i < length; i++) {
                        request = new main.Request(null, 'ping');
                        queue.push(request);
                        peer._sendBuffer.push(request);
                        request.toString = toString;
                    }
                    peer.send();
                    for (i = 0; i < queue.length; i++) {
                        test(queue[i], i);
                    }
                    peer.disconnect();
                    peer.once('disconnect', function () {
                        done();
                    });
                });
            });

        it('После отправки сообщения, оно удаляется из буфера Peer#_sendBuffer',
            function (done) {
                done();
            });

        it('Должен быть вызван сразу после установления подключения',
            function (done) {
                var peer = new main.Peer({url: url}),
                    called = false;

                peer.send = function () {
                    called = true;
                    delete peer.send;
                    return peer.send();
                };
                peer.connect();
                peer.on('connect', function () {
                    setTimeout(function () {
                        assert.ok(called, 'Метод не был вызван после установления подключения');
                        peer.disconnect();
                        peer.once('disconnect', function () {
                            done();
                        });
                    }, 0);
                });
            });
    });
});