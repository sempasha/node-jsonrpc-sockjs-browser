/**
 * @fileOverview Тест подключения по различным протоколам
 */

/* global describe, it */
var assert = require('assert'),
    main = require('../');

describe('Подключение по различным транспортым протоколам', function () {
    var test_proto = function (url, proto) {
            if (typeof proto === 'undefined') {
                proto = null;
            } else if (typeof proto === 'string') {
                proto = [proto];
            }
            return function (done) {
                var peer = new main.Peer({url: url, sockjs: {protocols_whitelist: proto}});
                peer.connect();
                peer.once('connect', function () {
                    if (proto !== null) {
                        if (proto.length === 1) {
                            assert.ok(proto[0] === peer._socket.protocol, 'Протокол "' + peer._socket.protocol +
                                '" не соответствует заданному "' + proto[0] + '"');
                        } else {
                            assert.ok(proto.indexOf(peer._socket.protocol) > -1, 'Протокол "' + peer._socket.protocol +
                                '" не соответствует заданному списку "' + proto.join('", "') + '"');
                        }
                    }
                    peer.disconnect();
                    peer.once('disconnect', function () {
                        done();
                    });
                });
            };
        },
        test_each_protocol = window.location.search.indexOf('each=') > -1,
        same_origin_url = window.location.protocol + '//' + window.location.host + '/service',
        remote_host_index = window.location.search.indexOf('remote_host='),
        remote_host = remote_host_index > -1 && window.location.search.slice(remote_host_index + 12),
        other_origin_url = remote_host && window.location.protocol + '//' + remote_host + '/service',
        protocols = [
            'websocket',
            'xhr-streaming',
            'xhr-polling',
            'xdr-streaming',
            'xdr-polling',
            'iframe-eventsource',
            'iframe-htmlfile',
            'iframe-xhr-polling',
            'jsonp-polling'
        ],
        i = 0;

    it('По любому протоколу из ' + protocols.join(', '), test_proto(same_origin_url, protocols));
    if (test_each_protocol) {
        for (i = 0; i < protocols.length; i++) {
            it('По ' + protocols[i], test_proto(same_origin_url, protocols[i]));
        }
    }

    if (other_origin_url) {
        it('Кроссдомен по любому протоколу из ' + protocols.join(', '), test_proto(other_origin_url, protocols));
        if (test_each_protocol) {
            for (i = 0; i < protocols.length; i++) {
                it('Кроссдомен по ' + protocols[i], test_proto(same_origin_url, protocols[i]));
            }
        }
    }
});