/**
 * @fileOverview Тест отправки запросов
 */

/* global describe, it, before, after */
var assert = require('assert'),
    main = require('../'),
    url = window.location.protocol + '//' + window.location.host + '/service';

describe('Функциональные тесты. JSON-RPC взаимодействие', function() {

    var peer;

    before(function (done) {
        assert.doesNotThrow(function () {
            peer = new main.Peer({url: url, reconnect_timeout: 500});
            peer.connect();
            peer.once('connect', function () {
                done();
            });
        });
    });

    after(function (done) {
        peer.disconnect();
        if (peer.isConnected()) {
            peer.once('disconnect', function () {
                done();
            });
        } else {
            done();
        }
    });

    it('Отправляем уведомление ping(payload=hello)',
        function (done) {
            if (peer.isConnected()) {
                test();
            } else {
                peer.once('connection', test);
            }
            function test () {
                peer.notify('ping', {payload: 'hello'}).onFinish(function (error, result) {
                    assert.ok(error === null, 'Произошла ошибка вызова процедуры: ' + error);
                    assert.ok(result === null, 'Результат должен быть пустым, так как это уведомление и результат ' +
                        'определяется после отправки сообщения удалённой стороне, и должен быть пустм в случае ' +
                        'успешной отправки');
                    done();
                });
            }
        });

    it('Отправляем запрос ping(payload=hello)',
        function (done) {
            if (peer.isConnected()) {
                test();
            } else {
                peer.once('connection', test);
            }
            function test () {
                var payload = 'hello';

                peer.request('ping', {payload: payload}).onFinish(function (error, result) {
                    assert.ok(error === null, 'Произошла ошибка вызова процедуры: ' + error);
                    assert.ok(result === payload, 'Результатом должна быть строка "' + payload + '"');
                    done();
                });
            }
        });

    it('Отправляем серию запросов ping(payload=N), где N номер запроса в серии',
        function (done) {
            if (peer.isConnected()) {
                test();
            } else {
                peer.once('connection', test);
            }
            function test () {
                var messages_to_send = 10,
                    messages_has_sent = 0,
                    messages_has_respond = 0,
                    send = function () {
                        var index = ++messages_has_sent;
                        peer.request('ping', {payload: index}).onFinish(function (error, result) {
                            messages_has_respond++;
                            assert.ok(error === null, 'Произошла ошибка вызова процедуры: ' + error);
                            assert.strictEqual(result, index, 'Получен неверный номер');
                            if (messages_to_send === messages_has_sent && messages_has_sent === messages_has_respond) {
                                done();
                            }
                        });
                    };

                while(messages_to_send > messages_has_sent) {
                    send();
                }
            }
        });

    it('Отправляем запрос sleep(ms=1000)',
        function (done) {
            if (peer.isConnected()) {
                test();
            } else {
                peer.once('connection', test);
            }
            function test () {
                var timeout = 1000,
                    start = Date.now();

                peer.request('sleep', {ms: 1000}).onFinish(function (error, result) {
                    var split = Date.now() - start;
                    assert.ok(error === null, 'Произошла ошибка вызова процедуры: ' + error);
                    assert.ok(true || Math.abs(split - timeout) < 50, 'Время ответа ' + split +
                        'мс не согласуется с параметром ms ' + timeout + 'мс');
                    done();
                });
            }
        });

    it('Отправляем серию запросов sleep(ms=X), где 0 <= X < 5000',
        function (done) {
            if (peer.isConnected()) {
                test.call(this);
            } else {
                peer.once('connection', test.bind(this));
            }
            function test () {
                var messages_to_send = 10,
                    messages_has_sent = 0,
                    messages_has_respond = 0,
                    max_timeout = 5000,
                    send = function () {
                        var timeout = Math.floor(max_timeout * Math.random()),
                            start = Date.now();

                        peer.request('sleep', {ms: timeout}).onFinish(function (error, result) {
                            var split = Date.now() - start;
                            messages_has_respond++;
                            assert.ok(error === null, 'Произошла ошибка вызова процедуры: ' + error);
                            assert.ok(true || Math.abs(split - timeout) < 50, 'Время ответа ' + split +
                                'мс не согласуется с параметром ms ' + timeout + 'мс');
                            if (messages_to_send === messages_has_sent && messages_has_sent === messages_has_respond) {
                                done();
                            }
                        });
                    };

                this.timeout(1000 + max_timeout);
                while(messages_to_send > messages_has_sent) {
                    messages_has_sent++;
                    send();
                }
            }
        });

    it('При разрыве соединения все исходящие/входящие запросы должны быть завершены с ошибкой "Connection closed" ' +
        '(код -32002)',
        function (done) {
            if (peer.isConnected()) {
                test();
            } else {
                peer.once('connection', test);
            }
            function test () {
                var outgoing,
                    incoming;

                peer.request('scheduleRequest', {
                    ms: 10,
                    method: 'sleep',
                    params: {ms: 10}
                }).onFinish(function (error, result) {
                    peer._getRequestedProcedure = function (method) {
                        if (method === 'sleep') {
                            return function (ms) {
                                if (typeof ms !== 'number' || ms % 1 !== 0) {
                                    throw new TypeError('Param ms must be an positive integer');
                                } else if (ms < 0) {
                                    throw new RangeError('Param ms must be an positive integer');
                                }
                                var callback,
                                    promise = {then: function (success) {callback = success;}},
                                    id;

                                setTimeout(function () {
                                    callback();
                                }, ms);

                                outgoing = peer.request('sleep', {ms: 100});
                                for (id in peer._incomingRequests) {
                                    incoming = peer._incomingRequests[id];
                                    break;
                                }

                                setTimeout(disconnect, 0);

                                return promise;
                            };
                        }
                    };
                });

                function disconnect () {
                    var processed = 0,
                        process = function (error, result) {
                            assert.ok(error instanceof Error, 'Ошибки не произошло');
                            assert.equal(error.code, -32002, 'Неверный код ошибки');
                            assert.equal(error.message, 'Connection closed', 'Неверное сообщение ошибки, должно быть ' +
                                '"Connection closed"');

                            if (this === outgoing) {
                                assert.ok(!(this.id in peer._outgoingRequests), 'Исходящий запрос не удёлен из списка');
                            }

                            if (this === incoming) {
                                assert.ok(!(this.id in peer._incomingRequests), 'Входящий запрос не удёлен из списка');
                            }

                            if (++processed === 2) {
                                done();
                            }
                        };

                    outgoing.onFinish(process);
                    incoming.onFinish(process);
                    peer._socket.close();
                }
            }
        });
});