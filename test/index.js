/**
 * @fileOverview Пакет тестов для модуля
 * @ignore
 */

require('./peer');
require('./common.protocols');
require('./common.requests');