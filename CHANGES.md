История изменений [node-jsonrpc-sockjs-browser](https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser)
---

Описание истории изменений в библиотеке
[node-jsonrpc-sockjs-browser](https://bitbucket.org/sempasha/node-jsonrpc-sockjs-browser).

## 0.1.6 (30 июля 2014)

 * Поменялся стиль оформления тестов

## 0.1.5 (29 июля 2014)

 * Обновлена библиотека [node-jsonrpc](https://bitbucket.org/sempasha/node-jsonrpc), теперь подстраховка на случай
 отсутствия в браузере `JSON` объекта - это забота нашей библиотеки, из node-jsonrpc удалена зависимость от JSON3
 * В тестах состороны сервера используется библиотека
 [node-jsonrpc-sockjs](https://bitbucket.org/sempasha/node-jsonrpc-sockjs)

## 0.1.4 (29 июля 2014)

 * Изменён стиль документирования - главный модуль библиотеки носит название `main`

## 0.1.3 (28 июля 2014)

 * Для совместимости со старыми браузерами используется альтернативное обращение к полям объектов, имена которых
 являются зарезирвированными словами, так например, вместо `object.function`, используется `object["function"]`

## 0.1.2 (28 июля 2014)

 * Обновлена библиотека [node-jsonrpc](https://bitbucket.org/sempasha/node-jsonrpc), теперь конструктор
 `node-jsonrpc.Peer` поддерживает параметр options.request_timeout, соотвествтенно конструтор `main.Peer` поддерживает
 этот параметр тоже

## 0.1.1 (24 июля 2014)

 * Обновлена библиотека [node-jsonrpc](https://bitbucket.org/sempasha/node-jsonrpc), метод
 `node-jsonrpc.Request.isFinished` помечен как `deprecated` и более не используется
 * Базовый функционал покрыт тестами

## 0.1.0 (23 июля 2014)

 * Обновлена библиотека [node-jsonrpc](https://bitbucket.org/sempasha/node-jsonrpc), теперь класс `node-jsonrpc.Peer`
 наследует интерфейс `EventEmitter`, соответственно класс `main.Peer` наследует этот интерфейс от родителя

## 0.0.2 (23 июля 2014)

 * Добавлена проверка параметра `options` для конструктора `Peer`
 * При вызове `main.Peer.disconnect` сбрасывается таймер переподключения
 * В случае, если клиент перестал пользоваться соединением, но оно ещё не было закрыто, то на сообщения,
 принятые с данного соединения будет отправлен ответ с ошибкой `"Connection closed"` (код `-32002`)

## 0.0.1 (20 июля 2014)

Первый релиз библиотеки, включающий в себя клиента [JSON-RPC](http://www.jsonrpc.org/specification) сервиса,
поддерживающего [SockJS](https://github.com/sockjs/sockjs-client) подключения.